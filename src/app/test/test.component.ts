import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TestEgibiliteService } from '../services/cpn/test-egibilite.service';
declare let $: any;

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  /***************************all variable **************************/
  calendarOption :any
  i:any=9
  testEgibFormGroup:FormGroup;
  addEventForm: FormGroup;
  elegible:any[]
   graphic:any[]
   montage:any[]
   development:any[]
   marketing:any[]
   transition:any[]

  get f() { return this.addEventForm.controls; }
  /***********************************life cycle *******************/
  constructor(private _formBuilder:FormBuilder,private testService:TestEgibiliteService) { 
    /*************************form  data contact************************/
    this.testEgibFormGroup = this._formBuilder.group({
      codeP: ['',[ Validators.required,Validators.pattern("[0-9 ]{5}")]],
      nomSoc: ['', Validators.required],
      activite: ['', Validators.required],
      status: ['', Validators.required],
      help: ['', Validators.required],
      personneSal: ['', Validators.required],
      turnover:[50, Validators.required],
      lastTurnover:[0, Validators.required],
      haveSite:['', Validators.required],
      haveCrm:['', Validators.required],
      liensite: ['', Validators.required],
      datesite: ['', Validators.required],
      siteVal: ['', Validators.required],
      dateCrm: ['', Validators.required],
      typeCRM: ['', Validators.required],
      typeERP: ['', Validators.required],
      typeSite: ['', Validators.required],
      crmDev: ['', Validators.required],
      agence: ['', Validators.required],
      budget: ['', Validators.required],
      service: ['', Validators.required],
      siret: ['', [ Validators.required,Validators.pattern("[0-9 ]{14}")]],
      siren: [''],
      naf: ['', Validators.required],
      adresse: ['', Validators.required],
      zipcode: ['',[ Validators.required,Validators.pattern("[0-9 ]{5}")]],
      region: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      prenom: ['', Validators.required],
      nom: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      departement: ['', Validators.required],
      phoneEntrep: ['', Validators.required],
      post: ['', Validators.required],
      contactID: ['', Validators.required],
      meetingType: ['', Validators.required],
    });

 /*************************form data event *******************************/
 this.addEventForm = this._formBuilder.group({
  title: ['', [Validators.required]],
  dateDebut: ['', [Validators.required]],
  cid:this.testEgibFormGroup.value.contactID
 })   
/*************************************calandreir ***************************/
this.calendarOption = {
  customButtons: {
    myCustomButton: {
      text: 'custom!',
      click: function () {
        alert('clicked the custom button!');
      }
    }
  },
  locale:"fr",
  initialView: 'dayGridMonth',
  //initialEvents: INITIAL_EVENTS, // alternatively, use the events setting to fetch from a feed
  weekends: true,
  editable: true,
  selectable: true,
  selectMirror: true,
  droppable: false,
  displayEventTime: true,
  disableDragging:false,
  timeZone: 'UTC',
  refetchResourcesOnNavigate: true,


  headerToolbar: {
    left: 'prev,next today',
    center: 'title',
    right: 'dayGridMonth,dayGridWeek,dayGridDay'
  },
  dayMaxEvents: true,
  events: [],
  dateClick: this.handleDateClick,
}


  }

ngOnInit(): void {
    this.getTransition()
          
}
/***************************************select date rendez vous *************************/
/*Show Modal with Forn on dayClick Event*/
handleDateClick() {
  console.log("dateselect")
}

sendRendvous(){
      
      this.testService.addEvents(this.addEventForm.value).subscribe(res=>{
        console.log('event',res)
      })
}


/***********************************generate lien zoom ****************/
generateZoomLink(){
  this.test.zoom.generating = true;
  this.test.zoom.generated = true;
  this.testService.addZoom({cid:this.testEgibFormGroup.value.contactID,type:this.testEgibFormGroup.value.meetingType})
  .subscribe(response=>{
    console.log('zoom',response)
    if(!response.data.error){
      this.test.zoom.generating = false;
      this.test.zoom.generated = true;
      alert({title: 'Zoom', message: response.data.message, type: 'success'});
      this.nextStep()
    } else {
      this.test.zoom.generating = false;
      this.test.zoom.generated = false;
      alert({title: 'Zoom', message: response.data.message, type: 'error'});
    }
  })

}




 /*********************** selection turnover ******************/
selectedRange(val){
  this.testEgibFormGroup.value.turnover=val
}
 /***********************select transition ******************/
 getTransition(){
  this.testService.getTransitions().subscribe(res=>{
   this.transition=  res?.data
   console.log("transition",this.transition)
   //  this.elegible=res?.data.filter(data=>data.category==='Services')
   //  this.graphic=res?.data.filter(data=>data.category==='Graphique')
    // this.montage=res?.data.filter(data=>data.category==='Montage')
    // this.marketing=res?.data.filter(data=>data.category==='Marketing')
    // this.development=res?.data.filter(data=>data.category==='Développement')
    // console.log("elegi",this.elegible)
    // console.log("suplimet",this.development)
 })
}


/*************************************services ***************************************/
selectService(val){
  console.log('service',val)
  this.testEgibFormGroup.get('service').setValue(val)
}

/**********************************************turnover ******************************************/

turn:number=1
incTurn(){
  this.turn++  
  this.testEgibFormGroup.get('turnover').setValue(this.turn)
}
decTurn(){
  this.turn--
  this.testEgibFormGroup.get('turnover').setValue(this.turn)
}

/**********************************************Last turnover ******************************************/
setLastTurnover(val){
  const step = this.test.active.step;
  let range = [
    val[0],
    val[1]
  ];
  console.log('val',val)
  console.log('range',range)
  this.testService.getServiceTurnover(range).subscribe(response=>{
    console.log('data',response)
    this.testEgibFormGroup.get('service').setValue(response.data.transition_id)
    this.testEgibFormGroup.get('lastTurnover').setValue(response.data.id)
    this.testEgibFormGroup.get('budget').setValue( Math.ceil(response.data.budget/100)*100)
  

    this.test.data[11].budget = Math.ceil(response.data.budget/100);
    
    this.test.data[11].min = Math.ceil(response.data.budget_min/100)*100;
    this.test.data[11].target = Math.ceil(response.data.budget/100)*100;
    this.test.data[11].max = Math.ceil(response.data.budget_max/100)*100;

  })
}

 /******************************************************** get naf with siret**********************************/
getNafCompany(siret){
  let siren = siret.substring(0,9);
  this.testEgibFormGroup.get('siren').setValue(siren);
  this.test.data[this.test.active.step].loading = true;
  if(siren.length == 9 && siret.length == 14){
    this.testService.getCompanySiren(this.testEgibFormGroup.value.siret).subscribe(response=>{
      console.log('siren',response)
      this.test.data[this.test.active.step].loading = false;
      this.testEgibFormGroup.get('naf').setValue(response.ape);
      this.testEgibFormGroup.get('naf').setValue(response.ape);
    });
  }
}

 /******************************************************** save client to database**********************************/
setContactForm(formDatas){
  this.testService.addContact({
      address:{
        advisorName:this.testEgibFormGroup.value.nomSoc,
        line:this.testEgibFormGroup.value.adresse,
        zipcode:this.testEgibFormGroup.value.zipcode,
        region:this.testEgibFormGroup.value.region,
        departement:this.testEgibFormGroup.value.departement,
        city:this.testEgibFormGroup.value.city,
        country:this.testEgibFormGroup.value.country
      },
      companies:{
        name:this.testEgibFormGroup.value.nomSoc,
        status:this.testEgibFormGroup.value.status,
        activity:this.testEgibFormGroup.value.activite,
        help:this.testEgibFormGroup.value.help,
        salaries:this.testEgibFormGroup.value.personneSal,
        siret:this.testEgibFormGroup.value.siret,
        siren:this.testEgibFormGroup.value.siren,
        naf:this.testEgibFormGroup.value.naf,
        phone:this.testEgibFormGroup.value.phoneEntrep,
        turnover:this.testEgibFormGroup.value.turnover,
        lastTurnover:this.testEgibFormGroup.value.lastTurnover
      },
      contacts:{
        firstName:this.testEgibFormGroup.value.nom,
        lastName:this.testEgibFormGroup.value.prenom,
        email:this.testEgibFormGroup.value.email,
        phone:this.testEgibFormGroup.value.phone,
        position:this.testEgibFormGroup.value.post,
        type:3,
        comment:''
      },
      development:{
        haveWebsite:this.testEgibFormGroup.value.haveSite,
        websiteType:this.testEgibFormGroup.value.typeSite,
        websiteValue:this.testEgibFormGroup.value.siteVal,
        websiteLink:this.testEgibFormGroup.value.liensite,
        websiteDate:"12/03/1995",
        haveCrm:this.testEgibFormGroup.value.haveCrm,
        crmType:this.testEgibFormGroup.value.typeCRM,
        crmDev:this.testEgibFormGroup.value.crmDev,
        crmName:this.testEgibFormGroup.value.nom,
        erpName:this.testEgibFormGroup.value.nom,
        crmDate:this.testEgibFormGroup.value.dateCrm,
        agencyName:this.testEgibFormGroup.value.agence
      },
      investment:{
        service:this.testEgibFormGroup.value.service,
        budget:this.testEgibFormGroup.value.budget,
        digitalTransitions:[]
      },
      contactID:'',
      meetingType:''
    
  })
  .subscribe(response=>{
    if(!response.error){
      this.testEgibFormGroup.get('contactID').setValue(response.cid)
    }
  })
}
 /********************************************************status**********************************/
 getstatus(data){
  console.log('activi',data)
  this.testEgibFormGroup.get('status').setValue(data)
  
}

  /******************************************nextmodule  *************************************************/
nextStep(){
  this.test.active.step+=1
}

nextSubStep(){
  this.test.active.subStep+=1
}
 /******************************************prevmodule  *************************************************/
 prevStep(){
  this.test.active.step-=1;
}


/**************************test elgible */
elgiblTest(){
  this.eleg=null
  this.nextStep();
}
/**********************is cpn **************/
isCpn(){
  console.log("data result", this.testEgibFormGroup.value)
  let budget = this.testEgibFormGroup.value.budget;
  let service = this.testEgibFormGroup.value.service;
  let region = this.testEgibFormGroup.value.region;
  let naf = this.testEgibFormGroup.value.naf;
  console.log("isopen", this.test.result.isOpen)
  console.log("isCpn", this.test.result.isCpn)
  console.log("isLoading", this.test.result.isLoading)

  /**************************calcule cpn *******************/
  console.log("is open false")
      this.test.result.isOpen = true;
      this.test.result.isLoading = true;
      console.log("service", this.testEgibFormGroup.value.service)
      console.log("budget", this.testEgibFormGroup.value.budget)
      this.testService.cpnGrant(service,budget)
      .subscribe(response=>{
        console.log("cpnGrant",response)

        this.test.result.cpn.id = response.id;
        this.test.result.cpn.amount = response.grants;
        this.test.result.cpn.originalPrice = response.original_price;
        this.test.result.cpn.sellPrice = response.sell_price;
        this.test.result.isLoading = false;
      })
      console.log("step", this.test.active.step)
      this.nextStep();
}
  /****************************************************** resultat de test **************************************/
  eleg:boolean=null
  showResult(){
  console.log("data result", this.testEgibFormGroup.value)
  let budget = this.testEgibFormGroup.value.budget;
  let service = this.testEgibFormGroup.value.service;
  let region = this.testEgibFormGroup.value.region;
  let naf = this.testEgibFormGroup.value.naf;
  console.log("isopen", this.test.result.isOpen)
  console.log("isCpn", this.test.result.isCpn)
  console.log("isLoading", this.test.result.isLoading)
  this.nextStep();
  /**************************calcule cpn *******************/
  console.log("is open false")
      this.test.result.isOpen = true;
      this.test.result.isLoading = true;
      this.testService.cpnGrant(service,budget)
      .subscribe(response=>{
        console.log("cpnGrant",response)

        this.test.result.cpn.id = response.id;
        this.test.result.cpn.amount = response.grants;
        this.test.result.cpn.originalPrice = response.original_price;
        this.test.result.cpn.sellPrice = response.sell_price;
        this.test.result.isLoading = false;
      })
      console.log("step", this.test.active.step)

  switch (this.test.result.isOpen) {
    case true:
      console.log("is open true")
      if(this.test.result.isCpn){
        console.log("is cpn true")
        this.test.result.isCpn = false;
        this.test.result.isLoading = true;
        this.testService.regionalGrant(region,budget,naf)
        .subscribe(response=>{
          console.log("regionalGrant",response)
             this.eleg=response.eligible
          if(response.eligible){
            this.test.result.regional.id = response.id;
            this.test.result.regional.eligible = response.eligible;
            this.test.result.regional.voucher = response.voucher;
            this.test.result.regional.amount = response.amount;
            this.test.result.regional.region = response.region;
            console.log("is eligible",this.test.active.step)
          } else {
            this.test.result.regional.eligible = response.eligible;
            this.test.result.regional.voucher = null;
            this.test.result.regional.amount = null;
            console.log("is not eligi",this.test.active.step)
          }
          this.test.result.isLoading = false;
        })
      /*  .catch(error=>{
          this.test.result.isLoading = false;
          console.log(error)
        });*/
      } else {
        console.log("is cpn false")
        this.setContactForm(this.test.formData);
        this.test.result.isCpn = true;
        this.test.result.isOpen = false;
        this.nextStep();
      }
      break;
    
  }
  console.log("test",this.test.result)

}
  /******************************************************chekform **************************************/
checkForm(step){
  console.log('step',step)
  let subStep = this.test.active.subStep;
  let subStepCat = this.test.active.subStepCat;
  switch (step) {
    case 0:
      this.nextStep();
      break;
    case 15:
      if(this.testEgibFormGroup.value.codeP==''){
        alert("champ est obligatoir");
        }else{
              this.nextStep();
        }
      break;
    case 2:
      if(this.testEgibFormGroup.value.activite==''){
             alert("champ est obligatoir");
      }else{
        this.nextStep();
      }
      break;
    case 3:
      if(this.testEgibFormGroup.value.status==''){
        alert("champ est obligatoir");
      }else{
        this.nextStep();
      }
      break;
    case 4:
      if(this.testEgibFormGroup.value.nomSoc==''){
        alert("champ est obligatoir");
      }else{
        this.nextStep();
      }
      break;
    case 5:
      if(this.testEgibFormGroup.value.turnover==''){
        alert("champ est obligatoir");
      }else{
        this.nextStep();
      }
      break;
    case 6:
      if(this.testEgibFormGroup.value.help==''){
        alert("champ est obligatoir");
      }else{
        this.nextStep();
      }
      break;
    case 7:
    
        this.setLastTurnover([5000,50000])
        this.nextStep();
      
      break;
    case 8:
     
        if(this.testEgibFormGroup.value.personneSal == "de 0 à 5 Personnes" || this.testEgibFormGroup.value.personneSal == "de 5 à 10 Personnes"){
          this.test.active.subStepCat = 1;
          this.test.active.subStep = 1;
          this.nextStep();
        } else {
          this.test.active.subStepCat = 2;
          this.test.active.subStep = 1;
          this.nextStep();
        }
     
      break;
    case 9:
      switch (subStepCat) {
        case 1:
          switch (subStep) {
            case 1:
          if(this.testEgibFormGroup.value.haveSite == "oui"){
                this.nextSubStep();
              } else {
                this.nextStep();
              };
              break;
            case 2:
             
              this.nextSubStep();
              break;
            case 3:
              this.nextSubStep();
              break;
            case 4:
              this.nextSubStep();
              break;
            case 5:
              this.nextStep();
              break;
          }
          break;
        case 2:
          switch (subStep) {
            case 1:    
              if(this.testEgibFormGroup.value.haveCrm == "oui"){
                this.nextSubStep();
              } else {
                this.test.active.subStepCat = 1;
              }; 
              break;
            case 2:
              this.nextSubStep();
              break;
            case 3:
               this.nextSubStep();
              break;
            case 4:
              this.nextSubStep();
              break;
            case 5:
              this.nextSubStep();
              break;
            case 6:
             
                this.test.active.subStepCat = 1;
                this.test.active.subStep = 1;
             
              break;
          }
          break;
      }
      break;
    case 10:
     
        this.nextStep();
      
      break;
    case 11:
      if(this.testEgibFormGroup.value.budget==''){
        alert("champ est obligatoir");
      }else{
        this.nextStep();
      }
      break;
    case 12:
      if(this.testEgibFormGroup.value.siret==''){
        alert("champ est obligatoir");
      }else{
        this.getNafCompany(this.testEgibFormGroup.value.siret);
        this.nextStep();
      }
      break;
    case 13:
      if(this.testEgibFormGroup.value.adresse=='' && this.testEgibFormGroup.value.zipcode=='' && this.testEgibFormGroup.value.region=='' 
      &&this.testEgibFormGroup.value.city=='' &&this.testEgibFormGroup.value.country=='' ){
        alert("champ est obligatoir");
      }else{
        this.nextStep();
      }
      break;
    case 14:
      if(this.testEgibFormGroup.value.nom==''&&this.testEgibFormGroup.value.prenom==''&&this.testEgibFormGroup.value.email==''
      &&this.testEgibFormGroup.value.phone==''&&this.testEgibFormGroup.value.phoneEntrep==''){
        alert("champ est obligatoir");
      }else{
        this.isCpn();
      }
    break;
    case 15:
    this.nextStep();
    break;
    case 16:
    this.nextStep();
    break;
    case 1:
      this.generateZoomLink()
      if(!this.test.zoom.generated){
        alert("genrate zoom est obligatoir");
      }else{
        this.nextStep();
      }
      break;
  }
}

/*****************************************test step **************************************/
test:any={
  active:{
    step:0,
    subStep:1,
    subStepCat:0,
    stepType:"form",
    popup:false,
    confirmed:false,
  },
  result:{
    isOpen:false,
    isLoading:false,
    isCpn:true,
    regional:{
      id:null,
      region:null,
      eligible:false,
      voucher:null,
      amount:null,
    },
    cpn:{
      id:null,
      amount:null,
      originalPrice:null,
      sellPrice:null,
    },
  },
  
  zoom:{
    generating:false,
    generated:false,
  },
  orientations:[],
  data:[
    {
      step:0,
      title:"Bienvenue"
    },
    {
      step: 1,
      title:"Renseigner le code postal",
    },
    {
      step:2,
      title:"Nom de l'entreprise"
    },
    {
      step:3,
      title:"Statut juridique"
    },
    {
      step:4,
      title:"Secteur d'activité",
      options:[],
    },
    {
      step:5,
      title:"Avez vous perdu du chiffre d'affaires pendant la crise sanitaire",
      labels:[
        "Baisse",
        "",
        "",
        "",
        "",
        "-50%",
        "",
        "",
        "",
        "",
        "Stable",
        "",
        "",
        "",
        "",
        "50%",
        "",
        "",
        "",
        "",
        "Hausse"
      ],
    },
    {
      step:6,
      title:"Avez vous déja obtenu des aides de l'état",
      options:[
        "Chéque numérique et aide numérique de votre région",
        "Crédit d'impôt",
        "Fond de solidarité",
        "Chaumage partiel",
        "Aucune aide",
      ],
    },
    {
      step:7,
      title:"Dernier chiffre d'affaires réalisé",
      labels:[
        "5k €",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "700k €",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "3.5m €",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "7.5m €",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "30m €",
      ],
      selectedRange:[0,1],
      range:[
        5000,
        50000,
        100000,
        200000,
        300000,
        400000,
        500000,
        600000,
        700000,
        800000,
        900000,
        1000000,
        1500000,
        2000000,
        2500000,
        3000000,
        3500000,
        4000000,
        4500000,
        5000000,
        5500000,
        6000000,
        6500000,
        7000000,
        7500000,
        8000000,
        8500000,
        9000000,
        9500000,
        10000000,
        15000000,
        20000000,
        25000000,
        30000000,
      ],
    },
    {
      step:8,
      title:"Nombre de salariés",
      options:[
        "de 0 à 5 Personnes",
        "de 5 à 10 Personnes",
        "de 10 à 20 Personnes",
        "de 20 à 30 Personnes",
        "de 30 à 40 Personnes",
        "de 40 à 50 Personnes",
        "plus de 50 Personnes",
      ],
    },
    {
      step:9,
      title:"Type de site",
      website:[
        {
          subStep:0,
          title:""
        },
        {
          subStep:1,
          title:"Avez vous un site internet pour votre entreprise"
        },
        {
          subStep:2,
          title:"Type de site"
        },
        {
          subStep:3,
          title:"Lien de site"
        },
        {
          subStep:4,
          title:"Date de développement"
        },
        {
          subStep:5,
          title:"L'agence qui a développé votre site"
        }
      ],
      crm:[
        {
          subStep:0,
          title:""
        },
        {
          subStep:1,
          title:"Avez vous un crm pour votre entreprise"
        },
        {
          subStep:2,
          title:"Type de crm"
        },
        {
          subStep:3,
          title:"Le crm a été développé"
        },
        {
          subStep:4,
          title:"Quel type de ERP vous utilisez",
          options:[
            "Zoho",
            "SAP",
            "Sage",
            "Oracle",
            "NetSuite",
            "Cegid",
            "Microsoft Dynamics",
            "Divalto",
            "WaveSoft",
            "Odoo",
            "Archipelia",
            "Axonaut",

          ]
        },
        {
          subStep:5,
          title:"Quel type de CRM vous utilisez",
          options:[
            "Zoho",
            "Habspot",
            "Microsoft Dynamics",
            "SalesForce",
            "Pipedrive",
            "Agile",
            "Axonaut",
            "FreshSales",
            "Teamleader",
            "Facture",
            "Crème",
            "Suite",
          ]
        },
        {
          subStep:6,
          title:"Date de développement"
        }
      ]
    },
    {
      step:10,
      title:"Quel projet est à subventionner pour votre transition numérique",
      tabServices:null,
      loading:false,
      services:["Services éligible", "Services suplémentaire"],
      tabCategories:null,
      categories:["Tous", "Graphique", "Développement","Montage","Marketing"],
      options:[],
    },
    {
      step:11,
      title:"Budget d'investissement",
      budget:5,
      min:400,
      target:500,
      max:100000,
    },
    {
      step:12,
      title:"Numéros d'identification",
      loading:false,
    },
    {
      step:13,
      title:"Adresse",
    },
    {
      step:14,
      title:"Fiche de renseignement",
      options:[
        "Gérant",
        "Directeur",
        "Associé",
        "Autre"
      ],
    },
    {
      step:15,
      title:"Vos disponibilités",
    },
    {
      step:16,
      title:"Type de client",
      items: ['☹️', '🙁', '😐', '🙂', '😊', '😍'],
      labels:[
        "agressif",
        "indécis",
        "anxieux",
        "économe",
        "compréhensif",
        "roi",
      ]
    },
    {
      step:17,
      title:"Merci pour votre temps",
    },
  ],
}




}
