import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-calandar',
  templateUrl: './calandar.component.html',
  styleUrls: ['./calandar.component.css']
})
export class CalandarComponent implements OnInit {

  currentDate:any
  constructor(private tokenStorage: TokenStorageService) {
   
   }

  ngOnInit(): void {
    this.currentDate=new Date()

   
  }
}
