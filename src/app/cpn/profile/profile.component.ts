import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import {AuthService} from 'src/app/services/cpn/auth.service'

declare let $: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  /********************** all variable *************/
  ModifierProfile: FormGroup;
  id :string ="13";
  profile:any;
  token:any=""
  user:any=null
  adresse:any
  socialMedia:any
  /**********************life Cycle ***********************/
    //Add user form actions
    get f() { return this.ModifierProfile.controls; }

  constructor(private auth:AuthService,private formBuilder: FormBuilder,private tokenStorage: TokenStorageService,private route: ActivatedRoute) {
    this.ModifierProfile = this.formBuilder.group({
      firstname: ['', [Validators.required]],
      lasttname: ['', [Validators.required]],
      email: ['', [Validators.required]],
      twitter:[''],
      instagram:[''],
      facebook:[''],
      teleph:[''],
      });
   }

  ngOnInit(): void {
  this.auth.getUser().subscribe(res=>{
      console.log('data send',res)
      this.profile=res
      this.user=res.data
      this.socialMedia=res.usersoc
      this.adresse=res.adress
            console.log('adress',res.adress)
      console.log('social',res.usersoc)
      console.log('user',res.data)

   })
  }

 /********************************update profile *****************************/
 onSubmit() {
         console.log('data send',this.ModifierProfile.value())

  if(this.ModifierProfile.value()){
   this.auth.updatUser(this.ModifierProfile.value()).subscribe(res=>{
      console.log('data send',res)
   })
  }
}

  openModal(){
    $('#cpnEditProfil').appendTo("body").modal('show')
   }

}
