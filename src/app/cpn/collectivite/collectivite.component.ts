import { Component, OnInit } from '@angular/core';
declare let $: any;

@Component({
  selector: 'app-collectivite',
  templateUrl: './collectivite.component.html',
  styleUrls: ['./collectivite.component.css']
})
export class CollectiviteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
       
  

    $('.search').mouseenter(function() {
        $(this).addClass('search--show');
        $(this).removeClass('search--hide');
    });

    $('.search').mouseleave(function() {
        $(this).addClass('search--hide');
        $(this).removeClass('search--show');
    });

    
  }

}
